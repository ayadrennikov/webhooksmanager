const yaml = require('js-yaml');
const https = require('https');

https.get('https://bitbucket.org/ayadrennikov/webhooksmanager/raw/HEAD/1.0.1/webhooks_manager.yaml', (resp) => {
  let data = '';

  resp.on('data', (chunk) => {
    data += chunk;
  });

  resp.on('end', () => {
    object = yaml.load(data);
    JSONDescription = JSON.stringify(object);
    console.log(JSONDescription);
  });

}).on("error", (err) => {
  throw new Error('Unable to convert');
});

